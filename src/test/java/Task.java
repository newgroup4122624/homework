import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.Duration;
import java.util.Iterator;
import java.util.List;
import java.util.Set;



public class Task {
   WebDriver driver;
   WebDriverWait wait;
   JavascriptExecutor js;

   @BeforeMethod
   public void setUp() {
      driver = new ChromeDriver();
      wait = new WebDriverWait(driver, Duration.ofSeconds(10));
      driver.get("https://demoqa.com/");
      driver.manage().window().maximize();
      driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
      js = (JavascriptExecutor) driver;
   }



   @Test
   public void task1() {
      String title = driver.getTitle();
      Assert.assertEquals(title, "DEMOQA", "True");
   }





   @Test
   public void task2(){
      driver.get("https://demoqa.com/");
      List<WebElement> cards = driver.findElements(By.cssSelector(".category-cards>div"));
      WebElement card = cards.get(0);
      js.executeScript("arguments[0].scrollIntoView();", card);
      card.click();
      List<WebElement> items = driver.findElements(By.cssSelector("ul>#item-0"));
      WebElement item = items.get(0);
      item.click();
      WebElement userName = driver.findElement(By.cssSelector("#userName"));
      userName.sendKeys("Ebru");
      WebElement userEmail = driver.findElement(By.cssSelector("#userEmail"));
      userEmail.sendKeys("ebrurustemova44@gmail.com");
      WebElement currentAddress = driver.findElement(By.cssSelector("#currentAddress"));
      currentAddress.sendKeys("Baku");
      WebElement permanentAddress = driver.findElement(By.cssSelector("#permanentAddress"));
      permanentAddress.sendKeys("Baku");
      WebElement submit = driver.findElement(By.cssSelector("#submit"));
      js.executeScript("arguments[0].scrollIntoView();", submit);
      submit.click();
      WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
      WebElement Name = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("name")));
      WebElement Email = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("email")));
      WebElement CurrentAddress = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("p#currentAddress")));
      WebElement PermanentAddress = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("p#permanentAddress")));
      Assert.assertEquals(Name.getText(), "Name:Ebru");
      Assert.assertEquals(Email.getText(), "Email:ebrurustemova44@gmail.com");
      Assert.assertEquals(CurrentAddress.getText(), "Current Address :Baku");
      Assert.assertEquals(PermanentAddress.getText(), "Permanent Address:Baku");
   }


   @Test
   public void task3(){
         driver.get("https://demoqa.com/");
         List<WebElement> cards = driver.findElements(By.cssSelector(".category-cards>div"));
         WebElement card = cards.get(0);
         js.executeScript("arguments[0].scrollIntoView();", card);
         card.click();
         List<WebElement> Boxs = driver.findElements(By.cssSelector("ul>#item-1"));
         WebElement checkBox = Boxs.get(0);
         checkBox.click();
         WebElement Btn = driver.findElement(By.cssSelector("button[title=\"Expand all\"]"));
         Btn.click();
         WebElement Icon = driver.findElement(By.cssSelector("label[for=\"tree-node-notes\"]>.rct-checkbox>svg"));
         Icon.click();
         WebElement result = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".text-success")));
         Assert.assertTrue(result.isDisplayed());
   }



   @Test
   public void task4(){
      driver.get("https://demoqa.com/");
      List<WebElement> cards = driver.findElements(By.cssSelector(".category-cards>div"));
      WebElement card = cards.get(0);
      js.executeScript("arguments[0].scrollIntoView();", card);
      card.click();
      List<WebElement> Buttons = driver.findElements(By.cssSelector("ul>#item-4"));
      WebElement Button = Buttons.get(0);
      Button.click();
      WebElement rightButton = driver.findElement(By.cssSelector("#rightClickBtn"));
      Actions action = new Actions(driver);
      action.contextClick(rightButton).build().perform();
      WebElement rightMessage = driver.findElement(By.cssSelector("#rightClickMessage"));
      Assert.assertTrue(rightMessage.isDisplayed());
   }



   @Test
   public void task5(){
      driver.get("https://demoqa.com/");
      List<WebElement> cards = driver.findElements(By.cssSelector(".category-cards>div"));
      WebElement card = cards.get(0);
      js.executeScript("arguments[0].scrollIntoView();", card);
      card.click();
      List<WebElement> listItems = driver.findElements(By.cssSelector("ul>#item-7"));
      WebElement upDown= listItems.get(0);
      js.executeScript("arguments[0].scrollIntoView();", upDown);
      upDown.click();
      WebElement uploadFile = driver.findElement(By.cssSelector("#uploadFile"));
      String path = "C:\\Users\\Ebru\\IdeaProjects\\Task3Media\\src\\main\\java\\org\\example";
      uploadFile.sendKeys(path);
      WebElement text = driver.findElement(By.cssSelector("#uploadFile"));
      wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#uploadFile")));
      Assert.assertTrue(text.isDisplayed());
   }



public void task6(){
   driver.get("https://demoqa.com/");
   List<WebElement> cards = driver.findElements(By.cssSelector(".category-cards>div"));
   WebElement card = cards.get(0);
   js.executeScript("arguments[0].scrollIntoView();", card);
   card.click();
   List<WebElement> listItems = driver.findElements(By.cssSelector("ul>#item-8"));
   WebElement dynamicProperties = listItems.get(0);
   dynamicProperties.click();
   WebElement visibleButton = driver.findElement(By.cssSelector("#visible"));
   wait.until(ExpectedConditions.visibilityOf(visibleButton));
   Assert.assertTrue(visibleButton.isDisplayed());

}


   @Test
   public void task7() {
      driver.get("https://demoqa.com/");
      List<WebElement> listCards = driver.findElements(By.cssSelector(".category-cards>div"));
      WebElement card = listCards.get(2);
      js.executeScript("arguments[0].scrollIntoView();", card);
      card.click();
      List<WebElement> listItems = driver.findElements(By.cssSelector("ul>#item-0"));
      WebElement Browser=listItems.get(2);
      Browser.click();
      WebElement button = driver.findElement(By.cssSelector("#tabButton"));
      button.click();
      String Window = driver.getWindowHandle();
      Set<String> Id = driver.getWindowHandles();
      Iterator<String> iterator = Id.iterator();
      while (iterator.hasNext()){
         String childWindow = iterator.next();
         if (!Window.equalsIgnoreCase(childWindow)){
            driver.switchTo().window(childWindow);
            WebElement h1 = driver.findElement(By.cssSelector("#sampleHeading"));
            System.out.println(h1.getText());
            driver.close();
         }
      }
   }




   @Test
   public  void test8(){
      driver.get("https://demoqa.com/");
      List<WebElement> alertsWindows= driver.findElements(By.cssSelector(".category-cards>div"));
      WebElement card = alertsWindows.get(2);
      js.executeScript("arguments[0].scrollIntoView();", card);
      card.click();
      List<WebElement> Alerts = driver.findElements(By.cssSelector("ul>#item-1"));
      WebElement alert = Alerts.get(1);
      alert.click();
      driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
      WebElement button = driver.findElement(By.cssSelector("#promtButton"));
      button.click();
      driver.switchTo().alert().sendKeys("hello");
      driver.switchTo().alert().accept();
      WebElement result = driver.findElement(By.cssSelector("#promptResult"));
      Assert.assertTrue(result.getText().contains("hello"), "hello");
   }




@Test
   public void test9(){
   List<WebElement> cards = driver.findElements(By.cssSelector(".category-cards>div"));
   WebElement cardAlerts = cards.get(2);
   js.executeScript("arguments[0].scrollIntoView();", cardAlerts);
   cardAlerts.click();
   List<WebElement> listItems = driver.findElements(By.cssSelector(".menu-list>#item-1"));
   WebElement alerts = listItems.get(1);
   alerts.click();
   WebElement button = driver.findElement(By.cssSelector("#confirmButton"));
   button.click();
   driver.switchTo().alert().dismiss();
   WebElement result = driver.findElement(By.cssSelector("#confirmResult"));
   Assert.assertTrue(result.getText().contains("Cancel"));
}


   @Test
   public void task10() {
      List<WebElement> cards = driver.findElements(By.cssSelector(".category-cards>div"));
      WebElement cardAlerts = cards.get(2);
      js.executeScript("arguments[0].scrollIntoView();", cardAlerts);
      cardAlerts.click();
      List<WebElement> listItems = driver.findElements(By.cssSelector(".menu-list>#item-2"));
      WebElement frames = listItems.get(1);
      frames.click();
      WebElement iframe = driver.findElement(By.cssSelector("iframe#frame1"));
      driver.switchTo().frame(iframe);
      WebElement h1 = driver.findElement(By.cssSelector("#sampleHeading"));
      Assert.assertTrue(h1.isDisplayed());
   }




   @Test()
   public void task11() {
      List<WebElement> cards = driver.findElements(By.cssSelector(".category-cards>div"));
      WebElement card= cards.get(4);
      js.executeScript("arguments[0].scrollIntoView();", card);
      card.click();
      List<WebElement> listItems = driver.findElements(By.cssSelector(".menu-list>#item-1"));
      WebElement select = listItems.get(3);
      js.executeScript("arguments[0].scrollIntoView();", select);
      select.click();
      WebElement grid = driver.findElement(By.cssSelector("a[id=\"demo-tab-grid\"]"));
      grid.click();
      List<WebElement> gridFive = driver.findElements(By.cssSelector("#row2>li"));
      WebElement five = gridFive.get(1);
      five.click();
      wait.until(ExpectedConditions.attributeToBe(five, "class", "list-group-item active list-group-item-action"));
      Assert.assertEquals(five.getAttribute("class"), "list-group-item active list-group-item-action", "dtcfvygbuhnijmok");
   }




   @Test()
   public void task12() {
      List<WebElement> cards = driver.findElements(By.cssSelector(".category-cards>div"));
      WebElement card= cards.get(4);
      js.executeScript("arguments[0].scrollIntoView();", card);
      card.click();
      List<WebElement> listItems = driver.findElements(By.cssSelector(".menu-list>#item-3"));
      WebElement drop= listItems.get(3);
      drop.click();
      WebElement drag= driver.findElement(By.cssSelector("#draggable"));
      WebElement drops = driver.findElement(By.cssSelector("#droppable"));
      Actions action = new Actions(driver);
      action.dragAndDrop(drag, drops).build().perform();
      List<WebElement> texts = driver.findElements(By.cssSelector("div[id=\"droppable\"]>p"));
      WebElement text= texts.get(0);
      Assert.assertEquals(text.getText(), "Dropped!", "erxtcyvbunijmk");
   }


   @AfterMethod(enabled = false)
   public void tearDown() {
      driver.quit();
   }
















}
